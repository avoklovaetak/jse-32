package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskViewByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "find task by index";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[SHOW TASK]");
        System.out.println("ENTER INDEX:");
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        showTask(task.get());
    }

    @NotNull
    @Override
    public String name() {
        return "task-view-by-index";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
