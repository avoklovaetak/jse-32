package ru.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by index";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new ObjectNotFoundException();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        @NotNull final Optional<Task> task = serviceLocator.getTaskService().changeOneStatusByIndex(userId, index, status);
        if (!task.isPresent()) throw new TaskNotFoundException();
    }

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

