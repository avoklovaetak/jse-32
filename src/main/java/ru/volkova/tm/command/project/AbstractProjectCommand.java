package ru.volkova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.entity.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus().getDisplayName());
    }

}
