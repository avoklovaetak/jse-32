package ru.volkova.tm.entity;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.entity.IWBS;

public final class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    public String toString() {
        return id + ": " + getName() + "; " +
                "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
