package ru.volkova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner implements Runnable {

    public static final String FILE_PATH = "./";

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 5;

    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (final AbstractCommand command: bootstrap.getCommandService().getArgsCommandsList())
            commands.add(command.name());
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void run(){
        final File file = new File(FILE_PATH);
        for (final File item: file.listFiles()) {
            if (!item.isFile()) continue;
            final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            System.out.println(fileName);
            bootstrap.parseCommand(fileName);
            item.delete();
        }
    }

}
