package ru.volkova.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("not started"),
    IN_PROGRESS("in progress"),
    COMPLETE("complete");

    @NotNull
    private final String displayName;

    Status(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
