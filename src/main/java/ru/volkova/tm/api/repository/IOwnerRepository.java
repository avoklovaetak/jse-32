package ru.volkova.tm.api.repository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractOwnerEntity;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void clear();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    Optional<E> findById(@NotNull String userId,@NotNull String id);

    @NotNull
    Optional<E> findOneByIndex(@NotNull String userId,@NotNull Integer index);

    @NotNull
    Optional<E> findOneByName(@NotNull String userId,@NotNull String name);

    void removeById(@NotNull String userId,@NotNull String id);

    void removeOneByIndex(@NotNull String userId,@NotNull Integer index);

    void removeOneByName(@NotNull String userId,@Nullable String name);

}
