package ru.volkova.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.entity.AbstractEntity;
import java.util.List;

import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    Optional<E> add(@NotNull E entity);

    void remove(@NotNull E entity);

    void addAll(@Nullable List<E> entities);

}
