package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@Nullable AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getArgsCommandsList();

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getCommands();

}
