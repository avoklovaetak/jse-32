package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;

import java.util.List;
import java.util.Optional;


public interface IUserService extends IService<User> {

    void clear();

    @NotNull
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    List<User> findAll();

    @NotNull
    Optional<User> findById(@Nullable String id);

    @NotNull
    Optional<User> findByLogin(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    boolean isLoginExists(@Nullable String login);

    void lockByEmail(@Nullable String email);

    void lockById(@Nullable String id);

    void lockByLogin(@Nullable String login);

    void remove(@NotNull User user);

    void removeByEmail(@Nullable String email);

    void removeById(@Nullable String id);

    void removeByLogin(@Nullable String login);

    @NotNull
    Optional<User> setPassword(@Nullable String userId, @Nullable String password);

    void unlockByEmail(@Nullable String email);

    void unlockById(@Nullable String id);

    void unlockByLogin(@Nullable String login);

    @NotNull
    Optional<User> updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
