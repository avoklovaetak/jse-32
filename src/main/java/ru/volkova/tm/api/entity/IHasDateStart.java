package ru.volkova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStart {

    @NotNull
    Date getDateStart();

    void setDateStart(@NotNull Date dateStart);

}
