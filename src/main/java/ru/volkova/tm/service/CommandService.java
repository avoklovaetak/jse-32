package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.ICommandRepository;
import ru.volkova.tm.api.service.ICommandService;
import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.system.UnknownArgumentException;
import ru.volkova.tm.exception.system.UnknownCommandException;

import java.util.Collection;

public class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgsCommandsList() {
        return commandRepository.getArgsCommandsList();
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new UnknownArgumentException();
        return commandRepository.getCommandByArg(arg);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new UnknownCommandException();
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

}
