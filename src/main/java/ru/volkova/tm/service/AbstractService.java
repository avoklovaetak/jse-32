package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.AbstractEntity;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

import java.util.List;

import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public @NotNull Optional<E> add(@NotNull final E entity) {
        repository.add(entity);
        return Optional.of(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        repository.remove(entity);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        repository.addAll(entities);
    }
}
