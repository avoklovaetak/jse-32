package ru.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(@NotNull String[] args) {
        @Nullable final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
